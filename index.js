class Escena1 extends Phaser.Scene {
    Item;
    puntaje=0;
    texto;


    preload() {
        this.load.image('fondo', 'assets/sprite/fondo.png');
        this.load.image('azul', 'assets/sprite/azul.png');
        this.load.spritesheet('player', 'assets/sprite/player.png', {frameWidth: 32, frameHeight:48});
        this.load.image('amarillo', 'assets/sprite/amarillo.png');
        this.load.image('img0', 'assets/sprite/img0.png');
        this.load.image('img1', 'assets/sprite/img1.png');
        this.load.image('img2', 'assets/sprite/img2.png');
        this.load.image('img3', 'assets/sprite/img3.png');
        this.load.image('img4', 'assets/sprite/img4.png');
        this.load.image('img5', 'assets/sprite/img5.png');
    }

    create() {

        this.fondo = this.add.sprite(this.game.renderer.width/2, this.game.renderer.height/2, 'fondo');
        //this.setScale(2.5);
        //this.lanzarBasura();
        this.amarillo=this.add.sprite(1300, 700, 'amarillo').setInteractive();
        this.azul=this.add.sprite(1200, 700, 'azul').setInteractive();
        this.player=this.add.sprite(100, 700, 'player');


        //cargar animaciones:
        this.anims.create({
            key: 'izquierda',
            frames: this.anims.generateFrameNumbers('player', {start:0, end:3}),
            frameRate:10,
            repeat: -1

        })

        this.anims.create({
            key: 'derecha',
            frames: this.anims.generateFrameNumbers('player', {start:5, end:8}),
            frameRate:10,
            repeat: -1

        })
        this.anims.create({
            key: 'quieto',
            frames: this.anims.generateFrameNumbers('player', {start:4, end:4}),
            frameRate:10,
            repeat: -1

        })


        this.teclas = this.input.keyboard.createCursorKeys();

        this.mostrarItem();

        this.amarillo.on("pointerdown", ()=>{
            this.destruirItem("amarillo");
        });

        this.azul.on("pointerdown", ()=>{
            this.destruirItem("azul");
        });

        const estilo = { font: "25px Arial Black", fill: "#ABDD84"};
        this.texto =this.add.text(20, 50, "PUNTAJE: "+this.puntaje,estilo);
        this.texto.setStroke('#00f', 10);
        this.texto.setShadow(3,8, "#333333", 2);



    }



    update(){

        this.texto.setText("PUNTAJE: " + this.puntaje);

        if (this.teclas.right.isDown){

            this.fondo.tilePositionX +=10
            this.player.x +=10
            this.player.anims.play('derecha', true);
            this.Item.x -=10

        }

        if (this.teclas.left.isDown){

            this.fondo.tilePositionX -=10
            this.player.x -=10
            this.player.anims.play('izquierda', true);
        }

        if (!this.teclas.right.isDown && !this.teclas.left.isDown){

            this.player.anims.play('quieto', true);
        }


    }






    destruirItem(categoria) {
        console.log(this.Item.categoria);
        console.log(categoria);
        console.log("destruir");
        if(this.Item.categoria==categoria){
            
            this.puntaje +=10
         

            console.log("coincide");
            this.Item.destroy();
            this.mostrarItem();

        }else {
            this.puntaje -=10
          this.Item.destroy();
          this.mostrarItem();
          }

    }



    mostrarItem(){
        //Math.random() * (max - min) + min;

        const aleatorio = Math.floor(Math.random() * 5);
        this.Item = this.physics.add.sprite(1100, 700, 'img' + aleatorio);
        console.log("aleatorio: ", aleatorio);
        this.Item.setCollideWorldBounds(true);
        this.Item.setScale(0.05);

        let nombre = '';
        let categoria = "";
        switch (aleatorio) {
        case 0:
        nombre = 'plastico';
        categoria = 'amarillo';
        break;
        case 1:
        nombre = 'lata';
        categoria = 'amarillo';
        break;
        case 2:
        nombre = 'lata1';
        categoria = 'amarillo';
        break;
        case 3:
        nombre = 'papel';
        categoria = 'azul';
        break;
        case 4:
        nombre = 'carton';
        categoria = 'azul';
        break;
        case 5:
        nombre = 'caja';
        categoria = 'azul';
        break;
        default:
        break;
        }
        this.Item.setName(nombre);
        this.Item.categoria = categoria;
        } //fin mostrarItem

}


const config = {
    type: Phaser.AUTO,
    width: 1366,
    height: 768,
    scene: [Escena1],
    physics: {
        default: 'arcade',
        arcade: {
            debug: true,
            gravity: {
                y: 200
            },
        },
    },
};

new Phaser.Game(config);
